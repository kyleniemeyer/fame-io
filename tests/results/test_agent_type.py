# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import string
from typing import Dict, List

import pytest
from fameprotobuf.Services_pb2 import Output

from fameio.source.results.agent_type import AgentTypeLog, AgentType
from tests.utils import assert_exception_contains


def create_types(count: int, upper: bool = False) -> Dict[str, Output.AgentType]:
    """Returns up to 25 AgentTypes with ascii letter names (optional in upper case)"""
    types = {}
    for i in range(0, count):
        agent_type = Output.AgentType()
        name = string.ascii_lowercase[i]
        agent_type.className = name.lower() if not upper else name.upper()
        types[agent_type.className] = agent_type
    return types


class TestAgentTypeLog:
    def test_init_none(self):
        agent_type_log = AgentTypeLog(None)  # noqa
        assert not agent_type_log.has_any_agent_type()

    def test_init_empty(self):
        agent_type_log = AgentTypeLog([])
        assert not agent_type_log.has_any_agent_type()

    def test_init_not_empty(self):
        agent_type_log = AgentTypeLog(["A", "B"])
        assert not agent_type_log.has_any_agent_type()

    def test_update_none(self):
        agent_type_log = AgentTypeLog([])
        agent_type_log.update_agents(None)  # noqa
        assert not agent_type_log.has_any_agent_type()

    def test_update_empty(self):
        agent_type_log = AgentTypeLog([])
        agent_type_log.update_agents({})
        assert not agent_type_log.has_any_agent_type()

    def test_update_no_filter_single_is_added(self):
        agent_type_log = AgentTypeLog([])
        new_types = create_types(1)
        agent_type_log.update_agents(new_types)
        assert agent_type_log.has_any_agent_type()
        for name, agent in new_types.items():
            assert agent_type_log.get_agent_type(name)._agent_type == agent

    def test_update_no_filter_multi_is_added(self):
        agent_type_log = AgentTypeLog([])
        new_types = create_types(3)
        agent_type_log.update_agents(new_types)
        assert agent_type_log.has_any_agent_type()
        for name, agent in new_types.items():
            assert agent_type_log.get_agent_type(name)._agent_type == agent

    def test_update_raise_on_overwrite(self):
        agent_type_log = AgentTypeLog([])
        first_types = create_types(3)
        agent_type_log.update_agents(first_types)
        second_types = {k: None for k in first_types.keys()}
        with pytest.raises(Exception) as e_info:
            agent_type_log.update_agents(second_types)
            assert_exception_contains(AgentTypeLog._ERR_DOUBLE_DEFINITION, e_info)

    def test_update_filter_accept_all(self):
        agent_types = create_types(3)
        agent_type_log = AgentTypeLog([k for k in agent_types.keys()])
        agent_type_log.update_agents(agent_types)
        for name, agent in agent_types.items():
            assert agent_type_log.get_agent_type(name)._agent_type == agent

    def test_update_filter_some(self):
        agent_types = create_types(5)
        accepted = [k for i, k in enumerate(agent_types.keys()) if i > 2]
        agent_type_log = AgentTypeLog(accepted)
        agent_type_log.update_agents(agent_types)
        self._assert_correct_split(agent_type_log, accepted, agent_types)

    @staticmethod
    def _assert_correct_split(
        type_log: AgentTypeLog, requested_types: List[str], all_types: Dict[str, Output.AgentType]
    ) -> None:
        for name in requested_types:
            assert type_log.get_agent_type(name)._agent_type == all_types[name]
        for name in [name for name in all_types.keys() if name not in requested_types]:
            with pytest.raises(Exception) as e_info:
                type_log.get_agent_type(name)
                assert_exception_contains(AgentTypeLog._ERR_AGENT_TYPE_MISSING, e_info)

    def test_update_filter_upper_lower_case(self):
        agent_types = create_types(5, upper=True)
        accepted = [name.lower() for i, name in enumerate(agent_types.keys()) if i > 2]
        agent_type_log = AgentTypeLog(accepted)
        agent_type_log.update_agents(agent_types)
        self._assert_correct_split(agent_type_log, [n.upper() for n in accepted], agent_types)

    def test_update_filter_lower_upper_case(self):
        agent_types = create_types(5, upper=False)
        accepted = [name.upper() for i, name in enumerate(agent_types.keys()) if i > 2]
        agent_type_log = AgentTypeLog(accepted)
        agent_type_log.update_agents(agent_types)
        self._assert_correct_split(agent_type_log, [n.lower() for n in accepted], agent_types)

    def test_is_requested_false_without_update(self):
        agent_type_log = AgentTypeLog([])
        assert not agent_type_log.is_requested("a")

    def test_is_requested_false_if_not_requested(self):
        agent_types = create_types(1)
        accepted = [k for k in agent_types.keys()]
        agent_type_log = AgentTypeLog(accepted)
        assert not agent_type_log.is_requested("NotRequested")

    def test_is_requested_false_if_requested_but_unregistered(self):
        agent_types = create_types(1)
        accepted = [k for k in agent_types.keys()]
        agent_type_log = AgentTypeLog(accepted)
        assert not agent_type_log.is_requested(accepted[0])

    def test_is_requested_true_if_requested_and_registered(self):
        agent_types = create_types(1)
        accepted = [k for k in agent_types.keys()]
        agent_type_log = AgentTypeLog(accepted)
        agent_type_log.update_agents(agent_types)
        assert agent_type_log.is_requested(accepted[0])

    def test_is_requested_true_if_registered(self):
        agent_types = create_types(1)
        agent_type_log = AgentTypeLog([])
        agent_type_log.update_agents(agent_types)
        names = [k for k in agent_types.keys()]
        assert agent_type_log.is_requested(names[0])


def create_type(name: str, fields: List[List[str]]) -> AgentType:
    """Creates a single protobuf Output.AgentType and wraps it"""
    agent_type = Output.AgentType()
    agent_type.className = name
    for field_id, names in enumerate(fields):
        pb_field = agent_type.field.add()
        pb_field.fieldId = field_id
        pb_field.fieldName = names[0]
        for i in range(1, len(names)):
            pb_field.indexName.extend([names[i]])
    return AgentType(agent_type)


class TestAgentType:
    def test_get_class_name(self):
        agent = create_type("MyCLassName", [["simpleFieldA"], ["simpleFieldB"]])
        assert agent.get_class_name() == "MyCLassName"

    def test_get_simple_column_map_no_fields_returns_empty_dict(self):
        agent = create_type("noFieldType", [])
        assert agent.get_simple_column_map() == {}

    def test_get_simple_column_map_return_all_simples(self):
        agent = create_type("simpleType", [["simpleFieldA"], ["simpleFieldB"]])
        assert agent.get_simple_column_map() == {0: "simpleFieldA", 1: "simpleFieldB"}

    def test_get_simple_column_map_return_ignores_complex(self):
        agent = create_type(
            "mixedType",
            [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"], ["complexField2", "columnB"]],
        )
        assert agent.get_simple_column_map() == {0: "simpleFieldA", 2: "simpleFieldB"}

    def test_get_merged_column_map_no_fields_returns_empty_dict(self):
        agent = create_type("noFieldType", [])
        assert agent.get_merged_column_map() == {}

    def test_get_merged_column_map_return_all_simples_as_is(self):
        agent = create_type("simpleType", [["simpleFieldA"], ["simpleFieldB"]])
        assert agent.get_merged_column_map() == {0: "simpleFieldA", 1: "simpleFieldB"}

    def test_get_merged_column_map_return_complex_merged(self):
        agent = create_type(
            "mixedType",
            [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"], ["complexField2", "columnB", "columnC"]],
        )
        assert agent.get_merged_column_map() == {
            0: "simpleFieldA",
            1: "complexField1_(('columnA',), value)",
            2: "simpleFieldB",
            3: "complexField2_(('columnB', 'columnC'), value)",
        }

    def test_get_simple_column_mask_all_simple(self):
        agent = create_type("simpleType", [["simpleFieldA"], ["simpleFieldB"]])
        assert agent.get_simple_column_mask() == [True, True]

    def test_get_simple_column_mask_all_complex(self):
        agent = create_type("complexType", [["complexField1", "columnA"], ["complexField2", "columnB", "columnC"]])
        assert agent.get_simple_column_mask() == [False, False]

    def test_get_simple_column_mask_mixed(self):
        agent = create_type(
            "mixedType",
            [["complexField1", "columnA"], ["simpleFieldA"], ["simpleFieldB"], ["complexField2", "columnB", "columnC"]],
        )
        assert agent.get_simple_column_mask() == [False, True, True, False]

    def test_get_complex_column_ids_empty_on_simples(self):
        agent = create_type("simpleType", [["simpleFieldA"], ["simpleFieldB"]])
        assert agent.get_complex_column_ids() == set()

    def test_get_complex_column_ids_returns_but_complex_columns(self):
        agent = create_type(
            "mixedType",
            [["complexField1", "columnA"], ["simpleFieldA"], ["simpleFieldB"], ["complexField2", "columnB", "columnC"]],
        )
        assert agent.get_complex_column_ids() == {0, 3}

    def test_get_column_name_for_id_returns_none_on_negative(self):
        agent = create_type("mixedType", [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"]])
        assert agent.get_column_name_for_id(-1) is None

    def test_get_column_name_for_id_returns_none_on_too_big(self):
        agent = create_type("mixedType", [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"]])
        assert agent.get_column_name_for_id(3) is None

    def test_get_column_name_for_id_returns_name_for_id(self):
        agent = create_type("mixedType", [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"]])
        assert agent.get_column_name_for_id(0) == "simpleFieldA"
        assert agent.get_column_name_for_id(1) == "complexField1"
        assert agent.get_column_name_for_id(2) == "simpleFieldB"

    def test_get_inner_columns_empty_on_simple_column(self):
        agent = create_type("mixedType", [["simpleFieldA"], ["complexField1", "columnA"], ["simpleFieldB"]])
        assert agent.get_inner_columns(0) == ()

    def test_get_inner_columns_names_on_complex_column(self):
        agent = create_type("mixedType", [["simpleFieldA"], ["complexField1", "a", "b", "c"], ["simpleFieldB"]])
        assert agent.get_inner_columns(1) == ("a", "b", "c")
