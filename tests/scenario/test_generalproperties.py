# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from fameio.source.scenario import GeneralProperties, ScenarioException

from tests.utils import assert_exception_contains


class TestGeneralProperties:
    def test_init_missing_run_id(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.run_id == 1

    def test_init_with_run_id(self):
        general = GeneralProperties.from_dict({"RunId": 66, "Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.run_id == 66

    def test_init_missing_simulation(self):
        with pytest.raises(ScenarioException) as e_info:
            GeneralProperties.from_dict({"RunId": 66})
        assert_exception_contains(GeneralProperties._MISSING_KEY, e_info)

    def test_init_missing_start_time(self):
        with pytest.raises(ScenarioException) as e_info:
            GeneralProperties.from_dict({"RunId": 66, "Simulation": {"StopTime": 0}})
        assert_exception_contains(GeneralProperties._MISSING_KEY, e_info)

    def test_init_missing_stop_time(self):
        with pytest.raises(ScenarioException) as e_info:
            GeneralProperties.from_dict({"RunId": 66, "Simulation": {"StartTime": 0}})
        assert_exception_contains(GeneralProperties._MISSING_KEY, e_info)

    def test_init_missing_random_seed(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.simulation_random_seed == 1

    def test_init_with_random_seed(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0, "RandomSeed": 87}})
        assert general.simulation_random_seed == 87

    def test_init_missing_output(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.output_process == 0
        assert general.output_interval == 100

    def test_init_with_output(self):
        general = GeneralProperties.from_dict(
            {
                "Simulation": {"StartTime": 0, "StopTime": 0},
                "Output": {"Process": 8, "Interval": 77},
            }
        )
        assert general.output_process == 8
        assert general.output_interval == 77
