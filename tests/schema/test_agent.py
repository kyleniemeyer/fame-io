# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

from typing import Any

import pytest

from fameio.source.schema.agenttype import AgentType
from fameio.source.schema.exception import SchemaException
from tests.utils import new_agent, new_attribute, assert_exception_contains


def make_agent_type(attributes: list, products: Any) -> AgentType:
    """Returns new AgentType with given attributes and products"""
    return AgentType.from_dict("DummyName", new_agent("DummyName", attributes, products)["DummyName"])


class Test:
    def test_init_no_product_and_attributes(self):
        assert make_agent_type([], [])

    def test_init_agent_no_products(self):
        assert make_agent_type([new_attribute("Dummy", "double", False, False, [])], [])

    def test_init_agent_no_attributes(self):
        assert make_agent_type([], ["Product1", "Product2"])

    def test_get_products_none(self):
        agent = make_agent_type([], None)
        assert isinstance(agent.products, list)
        assert len(agent.products) == 0

    def test_get_products_empty_list(self):
        agent = make_agent_type([], [])
        assert isinstance(agent.products, list)
        assert len(agent.products) == 0

    def test_get_products_no_list(self):
        with pytest.raises(SchemaException) as e_info:
            make_agent_type([], "NotAList")
        assert_exception_contains(AgentType._ERR_PRODUCTS_UNKNOWN_STRUCTURE, e_info)

    def test_get_products_list(self):
        agent = make_agent_type([], ["A", "B"])
        products = agent.products
        assert isinstance(products, list)
        assert "A" in products and "B" in products

    def test_get_products_list_not_strings(self):
        with pytest.raises(SchemaException) as e_info:
            make_agent_type([], [5.0, "AA"])
        assert_exception_contains(AgentType._ERR_PRODUCTS_NO_STRING_LIST, e_info)

    def test_get_products_dict(self):
        agent = make_agent_type([], {"A": 5, "B": {}})
        products = agent.products
        assert isinstance(products, list)
        assert "A" in products and "B" in products

    def test_get_attributes_empty(self):
        agent = make_agent_type([], [])
        assert isinstance(agent.attributes, dict)

    def test_get_attributes(self):
        agent = make_agent_type(
            [
                new_attribute("A", "double", False, False, []),
                new_attribute("B", "double", True, True, []),
            ],
            [],
        )
        attributes = agent.attributes
        assert isinstance(attributes, dict)
        assert "A" in attributes and "B" in attributes
