# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

from typing import List, Union, Any

import pytest

from fameio.source.schema import AttributeSpecs, SchemaException
from tests.utils import assert_exception_contains, new_attribute


def make_attribute(
    name: str,
    data_type: str = None,
    is_mandatory: bool = None,
    is_list: bool = None,
    nested_list: list = None,
    values: Any = None,
    default: Union[str, List[str]] = None,
) -> AttributeSpecs:
    """Returns new built attribute with given parameters"""
    definition = new_attribute(name, data_type, is_mandatory, is_list, nested_list, values, default)[name]
    return AttributeSpecs(name, definition)


class Test:
    def test_init_type_missing(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", None, False, False)
        assert_exception_contains(AttributeSpecs._MISSING_TYPE, e_info)

    def test_init_type_unknown(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "xxx", False, False)
        assert_exception_contains(AttributeSpecs._INVALID_TYPE, e_info)

    def test_init_type_lower_case(self):
        assert make_attribute("OIL", "double", False, False)

    def test_init_type_upper_case(self):
        assert make_attribute("OIL", "DOUBLE", False, False)

    def test_init_type_mixed_case(self):
        assert make_attribute("OIL", "dOUbLe", False, False)

    def test_init_mandatory_missing(self):
        attribute_type = make_attribute("OIL", "double", None, False)
        assert attribute_type.is_mandatory

    def test_init_mandatory_false(self):
        attribute_type = make_attribute("OIL", "double", False, False)
        assert not attribute_type.is_mandatory

    def test_init_list_missing(self):
        attribute_type = make_attribute("OIL", "double", False, None)
        assert not attribute_type.is_list

    def test_init_attribute_list_true(self):
        attribute_type = make_attribute("OIL", "double", False, True)
        assert attribute_type.is_list

    def test_init_attribute_list_but_time_series(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "time_series", False, True)
        assert_exception_contains(AttributeSpecs._LIST_DISALLOWED, e_info)

    def test_init_values_none(self):
        attribute_type = make_attribute("OIL", "double", False, False, None, None)
        assert attribute_type.values == []

    def test_init_values_empty(self):
        attribute_type = make_attribute("OIL", "double", False, False, None, [])
        assert attribute_type.values == []

    def test_init_values_list(self):
        values = ["A", 1.0, 2]
        attribute_type = make_attribute("OIL", "double", False, False, None, ["A", 1.0, 2])
        assert attribute_type.values == values

    def test_init_values_dict(self):
        attribute_type = make_attribute("OIL", "double", False, False, None, {"A": {"Inner": "stuff"}, 1.0: 0, 2: "F"})
        assert attribute_type.values == ["A", 1.0, 2]

    def test_ini_values_fail_tuple(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "double", False, False, None, ("A", "B"))
        assert_exception_contains(AttributeSpecs._VALUES_ILL_FORMAT, e_info)

    def test_ini_values_fail_primitive(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "double", False, False, None, "NotAllowed")
        assert_exception_contains(AttributeSpecs._VALUES_ILL_FORMAT, e_info)

    def test_has_nested_attributes_missing(self):
        attribute_type = make_attribute("OIL", "double", True, False)
        assert not attribute_type.has_nested_attributes

    def test_has_nested_attributes(self):
        attribute_type = make_attribute("OIL", "block", True, False, [new_attribute("Inner", "double", True, False)])
        assert attribute_type.has_nested_attributes

    def test_has_default_enum(self):
        attribute_type = make_attribute("OIL", "enum", True, False, None, None, "MyDefault")
        assert attribute_type.has_default_value
        assert attribute_type.values == []

    def test_has_default_double(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, "5.2")
        assert attribute_type.has_default_value

    def test_has_default_missing(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, None)
        assert not attribute_type.has_default_value

    def test_convert_to_data_type_integer(self):
        attribute_type = make_attribute("OIL", "integer", True, False, None, None, "5")
        assert attribute_type.default_value == 5

    def test_convert_to_data_type_integer_fail_float(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "integer", True, False, None, None, "5.2")
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_to_data_type_integer_fail_string(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "integer", True, False, None, None, "7h15 15 n07 an 1n75g5r")
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_to_data_type_double(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, "4.2")
        assert attribute_type.default_value == 4.2

    def test_convert_to_data_type_double_works_with_int(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, "42")
        assert attribute_type.default_value == 42

    def test_convert_to_data_type_double_fail_string(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "double", True, False, None, None, "7h15 15 n07 an 1n75g5r")
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_to_data_type_enum(self):
        attribute_type = make_attribute("OIL", "enum", True, False, None, ["A", "B", "C"], None)
        assert attribute_type.default_value is None
        assert attribute_type.values == ["A", "B", "C"]

    def test_convert_to_data_type_enum_not_allowed(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "enum", True, False, None, ["A", "B", "C"], "D")
        assert_exception_contains(AttributeSpecs._DEFAULT_DISALLOWED, e_info)

    def test_convert_to_data_type_time_series(self):
        attribute_type = make_attribute("OIL", "time_series", True, False, None, None, "99.2")
        assert attribute_type.default_value == 99.2

    def test_convert_to_data_type_time_series_fail_string(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "time_series", True, False, None, None, "7h15 15 n07 an 1n75g5r")
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_to_data_type_block_fail(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "block", True, False, None, None, "Anything")
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_list_fail_no_list(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "integer", True, True, None, None, "5")
        assert_exception_contains(AttributeSpecs._DEFAULT_NOT_LIST, e_info)

    def test_convert_list_fail_disallowed(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "integer", True, True, None, [1, 2, 3], ["5"])
        assert_exception_contains(AttributeSpecs._DEFAULT_DISALLOWED, e_info)

    def test_convert_list_fail_incompatible(self):
        with pytest.raises(SchemaException) as e_info:
            make_attribute("OIL", "integer", True, True, None, None, ["5.2"])
        assert_exception_contains(AttributeSpecs._DEFAULT_INCOMPATIBLE, e_info)

    def test_convert_list(self):
        attribute_type = make_attribute("OIL", "integer", True, True, None, None, ["1", "1", "2", "3"])
        assert [1, 1, 2, 3] == attribute_type.default_value
