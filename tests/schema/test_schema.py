# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from fameio.source.schema import Schema, SchemaException

from tests.utils import assert_exception_contains, new_agent, new_schema


class Test:
    def test_init_empty(self):
        with pytest.raises(SchemaException) as e_info:
            Schema.from_dict({})
        assert_exception_contains(Schema._AGENT_TYPES_MISSING, e_info)

    def test_init_key_word_mixed_caps(self):
        assert Schema.from_dict({"AgEntTYPeS": {}})

    def test_init_empty_agent_types(self):
        assert Schema.from_dict(new_schema([]))

    def test_init_adds_agent(self):
        schema = Schema.from_dict(new_schema([new_agent("EmptyAgent", [], [])]))
        assert "EmptyAgent" in schema.agent_types.keys()
